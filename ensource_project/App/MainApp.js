Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq';
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';
var Show2D = false; // Turn to true if want to display without terrain
var Show3D;
if (Show2D) {
    Show3D = false;
    var viewer = new Cesium.Viewer('cesiumContainer', {
        baseLayerPicker: false,
        //scene3DOnly: true
    });
} else {
    Show3D = true;
    // var viewer = new Cesium.Viewer('cesiumContainer', {
    //     terrainProvider : Cesium.createWorldTerrain(),
    //     baseLayerPicker: true,
    //     //scene3DOnly: true,
    //     //terrainExaggeration: 1.5,
    //     navigationHelpButton: false,
    //     selectionIndicator: false,
    //     shadows: true
    // });
    var viewer = new Cesium.Viewer('cesiumContainer', {
        baseLayerPicker: true,
        //scene3DOnly: true,
        //terrainExaggeration: 1.5,
        navigationHelpButton: false,
        selectionIndicator: false,
        shadows: true
    });
    // viewer.terrainProvider = new Cesium.CesiumTerrainProvider({
    //     url: 'https://assets.agi.com/stk-terrain/world',
    //     requestWaterMask: true, // required for water effects
    //     requestVertexNormals: false // required for terrain lighting
    // });
}
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;
// Basic Set Up for view Stoekach area
var initialPosition = new Cesium.Cartesian3.fromDegrees(9.199816, 48.770000, 1631.082799425431);
// Initial Position of Patricks
//var initialPosition = new Cesium.Cartesian3.fromDegrees(9.1812468, 48.7785923, 1631.082799425431);
var initialOrientation = new Cesium.HeadingPitchRoll.fromDegrees(7.1077496389876024807, -31.987223091598949054, 0.025883251314954971306);
var homeCameraView = {
    destination: initialPosition,
    orientation: {
        heading: initialOrientation.heading,
        pitch: initialOrientation.pitch,
        roll: initialOrientation.roll
    }
};
// Add some camera flight animation options
homeCameraView.duration = 2.0;
homeCameraView.maximumHeight = 2000;
homeCameraView.pitchAdjustHeight = 2000;
homeCameraView.endTransform = Cesium.Matrix4.IDENTITY;
// Override the default home button
viewer.homeButton.viewModel.command.beforeExecute.addEventListener(function (e) {
    e.cancel = true;
    viewer.scene.camera.flyTo(homeCameraView);
});
viewer.scene.camera.setView(homeCameraView);
// Set up clock and timeline.
viewer.clock.shouldAnimate = true; // default
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-06-25T13:04:49Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-06-25T15:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601("2018-06-25T13:04:49Z");
viewer.clock.multiplier = 0; // sets a speedup
//viewer.clock.clockStep = Cesium.ClockStep.SYSTEM_CLOCK_MULTIPLIER; // tick computation mode
viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; // loop at the end
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

// A Function to turn off terrain
var DestroyView = function () {
    viewer.destroy();
};
////////////////////////////////////////
//Add 3D City DB function to load glTF//
////////////////////////////////////////

var dataLayer;
var city;
////////////////////////////////////////
// Add 3D Citymodel (3D-Tiles)//
////////////////////////////////////////
var city;
//CesiumStyleChoice.style.display = "block";

city = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
    // url: './Data/Stuttgart3D_2/3dTiles',
    url: 'http://localhost:8000/App/Data/Stuttgart3D_2/3dTiles',
    maximumScreenSpaceError: 8 // default value
}));
var defaultStyle = new Cesium.Cesium3DTileStyle({
    color: "color('white')",
    show: true
});
city.style = defaultStyle;

var TurnOn3DT = function () {
    city.show = true;
    //CesiumStyleChoice.style.display = "none";
}
var TurnOff3DT = function () {
    city.show = false;
    //CesiumStyleChoice.style.display = "none";
}

//////////////////////////////////
// A Function to fly Somewhere////
//////////////////////////////////
var ToStutt = function () {
    viewer.camera.flyTo({
        destination: initialPosition,
        orientation: {
            heading: initialOrientation.heading,
            pitch: initialOrientation.pitch,
            roll: initialOrientation.roll
        }
    });
};


var geojsonOptions = {
    clampToGround: true
};

var neighborhoodsPromise = Cesium.GeoJsonDataSource.load('./Data/stoeckach_data_2/Blocks_final2.geojson', geojsonOptions);
// "./Data/stoeckach_data_2/Blocks_final.geojson"
//../ThirdParty/SampleWorkshop/sampleNeighborhoods.geojson

// Save an new entity collection of neighborhood data
var neighborhoods, neighborhoodEntities;
neighborhoodsPromise.then(function (dataSource) {
    // Add the new data as entities to the viewer
    viewer.dataSources.add(dataSource);
    neighborhoods = dataSource.entities;
    neighborhoods.show = false

    // Get the array of entities
    neighborhoodEntities = dataSource.entities.values;
    for (var i = 0; i < neighborhoodEntities.length; i++) {
        var entity = neighborhoodEntities[i];

        if (Cesium.defined(entity.polygon)) {

            // Use kml neighborhood value as entity name
            entity.name = entity.properties.Region_OID;
            // Set the polygon material to a random, translucent color
            entity.polygon.material = Cesium.Color.fromRandom({
                maximumRed : 0.5,
                maximumGreen : 0.5,
                minimumBlue : 0,
                alpha : 0.4
            });
            // Tells the polygon to color the terrain. ClassificationType.CESIUM_3D_TILE will color the 3D tileset, and ClassificationType.BOTH will color both the 3d tiles and terrain (BOTH is the default)
            entity.polygon.classificationType = Cesium.ClassificationType.TERRAIN;
            // Generate Polygon center
            var polyPositions = entity.polygon.hierarchy.getValue(Cesium.JulianDate.now()).positions;
            var polyCenter = Cesium.BoundingSphere.fromPoints(polyPositions).center;
            var offsetpix = new Cesium.Cartesian2(0, -20);
            polyCenter = Cesium.Ellipsoid.WGS84.scaleToGeodeticSurface(polyCenter);
            entity.position = polyCenter;
            // Generate labels
            entity.label = {
                text: entity.name,
                showBackground: true,
                scale: 0.6,
                horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                distanceDisplayCondition: new Cesium.DistanceDisplayCondition(10.0, 8000.0),
                pixelOffset: offsetpix,
                disableDepthTestDistance: 100.0
            };
        }
    }
});

var TurnOffCensus = function () {

    // viewer.entities.remove(neighborhoods);
    neighborhoods.show = false
};
var addCensus = function () {

    // viewer.entities.remove(neighborhoods);
    neighborhoods.show = true
};